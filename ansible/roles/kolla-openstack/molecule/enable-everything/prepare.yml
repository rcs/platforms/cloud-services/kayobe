---
- name: Prepare
  hosts: all
  gather_facts: false
  tasks:
    - name: Ensure ironic inspector kernel and ramdisk image directory exists
      local_action:
        module: file
        path: "{{ item | dirname }}"
        state: directory
        recurse: true
      with_items:
        - "{{ kolla_inspector_ipa_kernel_path }}"
        - "{{ kolla_inspector_ipa_ramdisk_path }}"

    # NOTE(mgoddard): Previously we were creating empty files for the kernel
    # and ramdisk, but this was found to cause ansible to hang on recent
    # versions of docker. Using non-empty files seems to resolve the issue.
    # See https://github.com/ansible/ansible/issues/36725.
    - name: Ensure ironic inspector kernel and ramdisk images exist
      local_action:
        module: copy
        content: fake image
        dest: "{{ item }}"
      with_items:
        - "{{ kolla_inspector_ipa_kernel_path }}"
        - "{{ kolla_inspector_ipa_ramdisk_path }}"

    - name: Ensure parent directories of configuration files exist
      file:
        path: "{{ kolla_extra_config_path }}/{{ item }}"
        state: directory
        recurse: yes
      delegate_to: localhost
      run_once: true
      with_items:
        - neutron
        - aodh
        # Check that subdirectories are handled correctly
        - prometheus/prometheus.yml.d

    - name: Ensure extra configuration files exist
      copy:
        content: |
          [extra-{{ item | basename }}]
          foo=bar
        dest: "{{ kolla_extra_config_path }}/{{ item }}"
      run_once: true
      delegate_to: localhost
      loop:
        - aodh.conf
        - barbican.conf
        - blazar.conf
        - ceilometer.conf
        - cinder.conf
        - cloudkitty.conf
        - designate.conf
        - glance.conf
        - global.conf
        - gnocchi.conf
        - grafana.ini
        - heat.conf
        - ironic.conf
        - ironic-inspector.conf
        - kafka.server.properties
        - keystone.conf
        - magnum.conf
        - manila.conf
        - murano.conf
        - backup.my.cnf
        - galera.cnf
        - masakari.conf
        - neutron.conf
        - neutron/ml2_conf.ini
        - nova.conf
        - octavia.conf
        - sahara.conf
        - zookeeper.cfg

    - name: Template extra custom config files
      # These correspond to globs defined in molecule.yml
      copy:
        content: "{{ item.content }}"
        dest: "{{ kolla_extra_config_path }}/{{ item.dest }}"
      run_once: true
      delegate_to: localhost
      with_items:
        - dest: aodh/dummy.yml
          content: |
            dummy_variable: 123
        - dest: aodh/dummy.ini
          content: |
            [dummy-section]
            dummy_variable = 123
        - dest: prometheus/prometheus.yml.d/dummy.yml
          content: |
            dummy_variable: 123
      loop_control:
        label: "{{ item.dest }}"
